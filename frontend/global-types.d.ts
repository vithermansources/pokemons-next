/**
 * Typ vlastnosti objektu
 */
type PropType<TObj, TProp extends keyof TObj> = TObj[TProp];