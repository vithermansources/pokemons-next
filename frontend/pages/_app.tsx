import { Toaster } from "react-hot-toast";
import type { AppProps } from "next/app";
import PokemonDialog from "../modules/shared/pokemons/dialog/PokemonDialog";

import "../styles/global.scss";

function App({ Component, pageProps }: AppProps) {
	return <>
		<Toaster
			position="bottom-right"
			toastOptions={{
				success: {
					iconTheme: {
						primary: "rgb(116, 192, 162)",
						secondary: "white",
					},
				},
				style: {
					borderRadius: 0
				},
			}}
		/>
		<PokemonDialog />
		<Component {...pageProps} />
	</>;
}
export default App;