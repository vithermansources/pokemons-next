/**
 * Pokemon component to show detail
 */

import { togglefavorite, playPokemon } from "./utils";
import style from "./PokemonDetail.module.scss";

import PokemonCard from "./PokemonCard";
import Image from "next/image";
import IconPlay from "../../../public/images/icon-play.png";
import IconHeart from "../../../public/images/heart.png";
import IconFullHeart from "../../../public/images/fullheart.png";


interface NumberRange {
	minimum: string;
	maximum: string;
}

interface PokemonSimple {
	id: string;
	name: string;
	isFavorite: boolean;
	image: string;
}

interface Pokemon extends PokemonSimple {
	types: string[];
	maxCP: number;
	maxHP: number;
	weight: NumberRange;
	height: NumberRange;
	evolutions: PokemonSimple[];
	image: string;
	sound: string;
}

export interface PokemonCardProps {
	pokemon: Pokemon;
	showEvolutions?: boolean;
	onChange?: () => void;
}

export default function PokemonDetail(props: PokemonCardProps) {
	function onChange() {
		if (props.onChange) {
			props.onChange();
		}
	}

	return (
		<div className={style["pokemon-detail"]}>
			<div className="text-center p-3 position-relative mb-3">
				<div className={style.play}>
					<Image
						width={30} height={30}
						src={IconPlay}
						onClick={_ => playPokemon(props.pokemon)} />
				</div>

				<Image
					width={400} height={400}
					src={props.pokemon.image} />

			</div>
			<div className="bg-gray">
				<div className="d-flex justify-content-between align-items-start mb-3 p-3">
					<div className="text-nowrap">
						<strong>{props.pokemon.name}</strong><br />
						{props.pokemon.types.join(", ")}
					</div>
					<div>
						<Image
							width={20} height={20}
							className={style.heart}
							src={props.pokemon.isFavorite ? IconFullHeart : IconHeart}
							onClick={async (e) => {
								e.preventDefault();
								await togglefavorite(props.pokemon);
								onChange();
							}}
						/>
					</div>
				</div>
				<div className="d-flex align-items-center">
					<div className={style["progress-line"] + " " + style["progress-line--cp"]}></div>
					<div className={style["progress-value"] + " ms-3"}>CP: {props.pokemon.maxCP}</div>
				</div>
				<div className="d-flex align-items-center mb-3">
					<div className={style["progress-line"] + " " + style["progress-line--hp"]}></div>
					<div className={style["progress-value"] + " ms-3"}>HP: {props.pokemon.maxHP}</div>
				</div>
				<div className={"d-flex justify-content-between " + (props.showEvolutions !== false ? "mb-3" : "")}>
					<div className={style["value-box"] + " " + style["value-box--right-border"]}>
						<strong>Weight</strong><br />
						<small>{props.pokemon.weight.minimum} &ndash; {props.pokemon.weight.maximum}</small>
					</div>
					<div className={style["value-box"]}>
						<strong>Height</strong><br />
						<small>{props.pokemon.height.minimum} &ndash; {props.pokemon.height.maximum}</small>
					</div>
				</div>
			</div>
			{
				props.showEvolutions !== false && props.pokemon.evolutions.length > 0 &&
				<div className={style.evolutions}>
					<div className="font-weight-bold">Evolutions</div>

					<div className="d-flex">
						{props.pokemon.evolutions.map(evolution =>
							<PokemonCard pokemon={evolution} key={evolution.id}
								onChange={onChange} className={style["evolution-card"] + " me-2"} />
						)}
					</div>
				</div>
			}
		</div >
	);
}