/**
 * Shared pokemon's model
 */
import { makeAutoObservable } from "mobx"
import * as grapqlApi from "../../api/api-types-generated";
import api from "../../api/api";

type Pokemon = PropType<grapqlApi.LoadPokemonByIdQuery, "pokemonById">;

class PokemonDialog {
	open = false;
	pokemon: Pokemon;
	onChange?: () => void;

	constructor() {
		makeAutoObservable(this);
	}

	private loadPokemon = async (pokemonId: string) => {
		const pokemon = await api.LoadPokemonById({ id: pokemonId });
		this.pokemon = pokemon.pokemonById;
	}

	show = async (pokemonId: string, onChange?: () => void) => {
		await this.loadPokemon(pokemonId);
		this.onChange = onChange;
		this.open = true;
	}

	close = async () => {
		this.open = false;
	}

	triggerChange = async () => {
		if (this.onChange) this.onChange();
		if (this.pokemon) this.loadPokemon(this.pokemon.id);
	}
}

const pokemonDialog = new PokemonDialog();
export default pokemonDialog;