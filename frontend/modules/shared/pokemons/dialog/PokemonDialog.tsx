import { observer } from "mobx-react";
import { Modal } from 'react-responsive-modal';
import pokemonDialog from "./model";
import PokemonDetail from "../PokemonDetail";

export default observer(function PokemonDetailDialog() {
	return (
		<Modal open={pokemonDialog.open} onClose={pokemonDialog.close}>
			{pokemonDialog.pokemon &&
				<PokemonDetail
					pokemon={pokemonDialog.pokemon}
					showEvolutions={false}
					onChange={pokemonDialog.triggerChange}
				/>
			}
		</Modal >
	);
});