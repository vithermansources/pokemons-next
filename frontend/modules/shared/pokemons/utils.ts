/**
 * Pokemon's utility library
 */
import * as notification from "../notification";
import api from "../api/api";

export async function togglefavorite(pokemon: { id: string, isFavorite: boolean, name: string }) {
	if (pokemon.isFavorite) {
		await api.UnFavoritePokemon({ id: pokemon.id });
		notification.successMessage(`${pokemon.name} was removed from favorites`);
	} else {
		await api.FavoritePokemon({ id: pokemon.id });
		notification.successMessage(`${pokemon.name} was added to favorites`);
	}
}

export async function playPokemon(pokemon: { sound: string }) {
	const sound = new Audio(pokemon.sound);
	await sound.play();
}
