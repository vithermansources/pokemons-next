/**
 * Pokemon card component to show in list page
 */

import pokemonDialog from "./dialog/model";
import { togglefavorite } from "./utils";
import style from "./PokemonCard.module.scss";

import Image from "next/image";
import IconEye from "../../../public/images/icon-eye.png";
import IconHeart from "../../../public/images/heart.png";
import IconFullHeart from "../../../public/images/fullheart.png";

interface Pokemon {
	id: string;
	name: string;
	isFavorite: boolean;
	types?: string[];
	image: string;
}

export interface PokemonCardProps {
	className?: string;
	pokemon: Pokemon;
	onChange: () => void;
}

export default function PokemonCard(props: PokemonCardProps) {

	function onChange() {
		if (props.onChange) {
			props.onChange();
		}
	}

	return (
		<div className={style.card + " " + (props.className ?? "")}>
			<div className={style.eye}>
				<Image src={IconEye} width={30} height={30} onClick={async e => {
					e.stopPropagation();
					await pokemonDialog.show(props.pokemon.id, onChange);
				}} />
			</div>
			<div className="d-flex align-items-center justify-content-center h-100 p-4">
				<Image width={150} height={150} src={props.pokemon.image} />
			</div>
			<div className="d-flex justify-content-between align-items-center p-2 bg-gray">
				<div className="text-nowrap">
					<strong>{props.pokemon.name}</strong><br />
					{props.pokemon.types?.join(", ")}
				</div>
				<div>
					<div className={style.heart}>
						<Image
							src={props.pokemon.isFavorite ? IconFullHeart : IconHeart}
							width={20} height={20}
							onClick={async (e) => {
								e.stopPropagation();
								await togglefavorite(props.pokemon);
								onChange();
							}}
						/>
					</div>

				</div>
			</div>
		</div>
	);
}