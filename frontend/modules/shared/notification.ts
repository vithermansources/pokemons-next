/*
 * Knihovna pro zobrazení notifikačních zpráv.
 */

import toast from "react-hot-toast";

/**
 * Zpráva o úspěšně provedené operaci.
 */
export function successMessage(message: string) {
	toast.success(message);
}

/**
 * Zpráva o chybě.
 */
export function errorMessage(message: string) {
	toast.error(message);
}

/**
 * Loading message.
 */
export function loadingMessage(message: string) {
	return toast.loading(message);
}

/**
 * Dismiss loading message. 
 */
export function dismissLoading(messageId: string) {
	return toast.dismiss(messageId);
}