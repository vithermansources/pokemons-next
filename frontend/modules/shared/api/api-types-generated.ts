import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Attack = {
  name: Scalars['String'];
  type: Scalars['String'];
  damage: Scalars['Int'];
};

export type Mutation = {
  favoritePokemon?: Maybe<Pokemon>;
  unFavoritePokemon?: Maybe<Pokemon>;
};


export type MutationFavoritePokemonArgs = {
  id: Scalars['ID'];
};


export type MutationUnFavoritePokemonArgs = {
  id: Scalars['ID'];
};

export type Pokemon = {
  id: Scalars['ID'];
  number: Scalars['Int'];
  name: Scalars['String'];
  weight: PokemonDimension;
  height: PokemonDimension;
  classification: Scalars['String'];
  types: Array<Scalars['String']>;
  resistant: Array<Scalars['String']>;
  attacks: PokemonAttack;
  weaknesses: Array<Scalars['String']>;
  fleeRate: Scalars['Float'];
  maxCP: Scalars['Int'];
  evolutions: Array<Pokemon>;
  evolutionRequirements?: Maybe<PokemonEvolutionRequirement>;
  maxHP: Scalars['Int'];
  image: Scalars['String'];
  sound: Scalars['String'];
  isFavorite: Scalars['Boolean'];
};

export type PokemonAttack = {
  fast: Array<Attack>;
  special: Array<Attack>;
};

export type PokemonConnection = {
  limit: Scalars['Int'];
  offset: Scalars['Int'];
  count: Scalars['Int'];
  edges: Array<Pokemon>;
};

export type PokemonDimension = {
  minimum: Scalars['String'];
  maximum: Scalars['String'];
};

export type PokemonEvolutionRequirement = {
  amount: Scalars['Int'];
  name: Scalars['String'];
};

export type PokemonFilterInput = {
  type?: Maybe<Scalars['String']>;
  isFavorite?: Maybe<Scalars['Boolean']>;
};

export type PokemonsQueryInput = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  search?: Maybe<Scalars['String']>;
  filter?: Maybe<PokemonFilterInput>;
};

export type Query = {
  pokemons: PokemonConnection;
  pokemonByName?: Maybe<Pokemon>;
  pokemonById?: Maybe<Pokemon>;
  pokemonTypes: Array<Scalars['String']>;
};


export type QueryPokemonsArgs = {
  query: PokemonsQueryInput;
};


export type QueryPokemonByNameArgs = {
  name: Scalars['String'];
};


export type QueryPokemonByIdArgs = {
  id: Scalars['ID'];
};

export type Root = {
  query: Query;
};

export type PokemonListItemFragment = Pick<Pokemon, 'id' | 'name' | 'types' | 'isFavorite' | 'image' | 'sound'>;

export type PokemonDetailFragment = (
  Pick<Pokemon, 'id' | 'name' | 'types' | 'isFavorite' | 'maxCP' | 'maxHP' | 'image' | 'sound'>
  & { weight: Pick<PokemonDimension, 'minimum' | 'maximum'>, height: Pick<PokemonDimension, 'minimum' | 'maximum'>, evolutions: Array<Pick<Pokemon, 'id' | 'name' | 'isFavorite' | 'image'>> }
);

export type LoadPokemonsListQueryVariables = Exact<{
  filter: PokemonsQueryInput;
}>;


export type LoadPokemonsListQuery = { pokemons: (
    Pick<PokemonConnection, 'limit' | 'offset' | 'count'>
    & { edges: Array<PokemonListItemFragment> }
  ) };

export type LoadPokemonByNameQueryVariables = Exact<{
  name: Scalars['String'];
}>;


export type LoadPokemonByNameQuery = { pokemonByName?: Maybe<PokemonDetailFragment> };

export type LoadPokemonByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type LoadPokemonByIdQuery = { pokemonById?: Maybe<PokemonDetailFragment> };

export type LoadPokemonTypesQueryVariables = Exact<{ [key: string]: never; }>;


export type LoadPokemonTypesQuery = Pick<Query, 'pokemonTypes'>;

export type FavoritePokemonMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type FavoritePokemonMutation = { favoritePokemon?: Maybe<Pick<Pokemon, 'isFavorite'>> };

export type UnFavoritePokemonMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type UnFavoritePokemonMutation = { unFavoritePokemon?: Maybe<Pick<Pokemon, 'isFavorite'>> };

export const PokemonListItemFragmentDoc = gql`
    fragment pokemonListItem on Pokemon {
  id
  name
  types
  isFavorite
  image
  sound
}
    `;
export const PokemonDetailFragmentDoc = gql`
    fragment pokemonDetail on Pokemon {
  id
  name
  types
  isFavorite
  weight {
    minimum
    maximum
  }
  height {
    minimum
    maximum
  }
  maxCP
  maxHP
  evolutions {
    id
    name
    isFavorite
    image
  }
  image
  sound
}
    `;
export const LoadPokemonsListDocument = gql`
    query LoadPokemonsList($filter: PokemonsQueryInput!) {
  pokemons(query: $filter) {
    limit
    offset
    count
    edges {
      ...pokemonListItem
    }
  }
}
    ${PokemonListItemFragmentDoc}`;
export const LoadPokemonByNameDocument = gql`
    query LoadPokemonByName($name: String!) {
  pokemonByName(name: $name) {
    ...pokemonDetail
  }
}
    ${PokemonDetailFragmentDoc}`;
export const LoadPokemonByIdDocument = gql`
    query LoadPokemonById($id: ID!) {
  pokemonById(id: $id) {
    ...pokemonDetail
  }
}
    ${PokemonDetailFragmentDoc}`;
export const LoadPokemonTypesDocument = gql`
    query LoadPokemonTypes {
  pokemonTypes
}
    `;
export const FavoritePokemonDocument = gql`
    mutation FavoritePokemon($id: ID!) {
  favoritePokemon(id: $id) {
    isFavorite
  }
}
    `;
export const UnFavoritePokemonDocument = gql`
    mutation UnFavoritePokemon($id: ID!) {
  unFavoritePokemon(id: $id) {
    isFavorite
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    LoadPokemonsList(variables: LoadPokemonsListQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoadPokemonsListQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<LoadPokemonsListQuery>(LoadPokemonsListDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'LoadPokemonsList');
    },
    LoadPokemonByName(variables: LoadPokemonByNameQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoadPokemonByNameQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<LoadPokemonByNameQuery>(LoadPokemonByNameDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'LoadPokemonByName');
    },
    LoadPokemonById(variables: LoadPokemonByIdQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoadPokemonByIdQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<LoadPokemonByIdQuery>(LoadPokemonByIdDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'LoadPokemonById');
    },
    LoadPokemonTypes(variables?: LoadPokemonTypesQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<LoadPokemonTypesQuery> {
      return withWrapper((wrappedRequestHeaders) => client.request<LoadPokemonTypesQuery>(LoadPokemonTypesDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'LoadPokemonTypes');
    },
    FavoritePokemon(variables: FavoritePokemonMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<FavoritePokemonMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<FavoritePokemonMutation>(FavoritePokemonDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'FavoritePokemon');
    },
    UnFavoritePokemon(variables: UnFavoritePokemonMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UnFavoritePokemonMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<UnFavoritePokemonMutation>(UnFavoritePokemonDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'UnFavoritePokemon');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;