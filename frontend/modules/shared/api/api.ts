import { ExecutionResult } from "graphql";
import { GraphQLClient } from "graphql-request";
import * as graphqlApi from "./api-types-generated";
import { withIndication } from "../common";
import * as notification from "../notification";

/**
 * Wrapper for API calls ensuring toastr message for errors
 */
const wrapper: graphqlApi.SdkFunctionWrapper = async (action, operationName) => {
	try {
		let toastId: string = "";
		return await withIndication({
			exec: action,
			indicateStart: () => {
				toastId = notification.loadingMessage(`Waiting for server operation ${operationName}...`);
			},
			finish: () => {
				notification.dismissLoading(toastId);
			}
		});
	} catch (e) {
		if (e.response && e.response.errors) {
			// API regular error (HTTP connection OK)
			const message = (e.response as ExecutionResult).errors?.map(e => e.message).join(" ");
			if (message && message.trim().length > 0) {
				notification.errorMessage(message);
			}
		} else {
			// HTTP connection failed
			const message = "Network connection to server failed. [Operation: " + operationName + "]";
			notification.errorMessage(message);
		}
		throw e;
	}
};

/**
 * Export of configured instance of generated API
 */
export default graphqlApi.getSdk(new GraphQLClient("https://pokemons-graphql-server.herokuapp.com/graphql"), wrapper);