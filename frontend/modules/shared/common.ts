import { useEffect, DependencyList } from "react";

/*
 * Univerzální sdílená klienta pro sdílené samostatné funkce mimo vlastní moduly.
 */

/**
 *  HOC funkce pro indikaci průběhu asynchronní operace execFunc(). Indikaci nastavuje
 *  funkce indicateStart() a ukončuje indicateFinish(). Funkce indicateStart() je volána se
 *  zpožděním 500ms.
 */
export async function withIndication<ExcecResult>(options: IndicationOptions<ExcecResult>) {
	if (options.start) {
		options.start();
	}

	const timeOut = options.indicateStart ? setTimeout(options.indicateStart, 500) : undefined;

	try {
		return await options.exec();
	} finally {
		if (timeOut) {
			clearTimeout(timeOut);
		}
		if (options.finish) {
			options.finish();
		}
	}
}

interface IndicationOptions<ExecResult> {
	/**
	 * Výkonná funkce
	 */
	exec: () => Promise<ExecResult>;

	/**
	 * Tato funkce bude zavolána a dokončena vždy před výkonnou funkcí.
	 */
	start?: () => void;

	/**
	 * Tato funkce bude zavolána se spožděním 500ms. Výkonná funkce v tu dobu již může být zpožděna.
	 */
	indicateStart?: () => void;

	/**
	 * Finalizační funkce. Bude zavolána po dokončení nebo po výjimce ve výkonné funkci.
	 */
	finish?: () => void;
}

/**
 *  Vrací HOC funkci, která volání předané funkce fn odloží až po ukočení případného sledu volání.
 *  Sledem přitom rozumíme řadu opakovaného volání s kratšími prodlevami, než parametr wait.
 */
export function debounce(fn: () => void, wait: number, context?: any): () => void {
	let timeout: any = null;
	let callbackArgs: any = null;

	const later = () => fn.apply(context, callbackArgs);

	return function () {
		callbackArgs = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
	};
}

/**
 * Pomocná funkce do příkazů switch pro exhaustivní statickou kontrolu ošetření všech větví.
 */
export function unwanted(v: never): never {
	throw new Error("Unwanted reached place");
}

/**
 * Obálka pro ignorování varování neošetřeného výsledku asynchronní funkce. 
 */
export function ignorePromises(...promises: Promise<any>[]) { }

/**
 * Async versin of useEffect hook 
 */
export function useEffectAsync(asyncFunc: () => Promise<any>, deps?: DependencyList) {
	useEffect(() => {
		ignorePromises(asyncFunc());
	}, deps);
}