/**
 * Pokemons list page
 */
import { useEffectAsync } from "../../shared/common";
import { observer } from "mobx-react";

import { useRouter } from "next/router";
import Image from "next/image";

import model from "./model";
import style from "./page.module.scss";

import PokemonCard from "../../shared/pokemons/PokemonCard";
import Paginator from "./Paginator";
import PokemonListItem from "./PokemonListItem";
import IconList from "../../../public/images/icon-list.png";
import IconGrid from "../../../public/images/icon-grid.png";

export default observer(function PokemonsListPage() {
	const router = useRouter();

	useEffectAsync(async () => {
		await model.fetch();
	}, []);

	const pokemons = model.pokemons;

	return (
		<div className="container">
			<div className="mb-4">
				<div className="mb-2">
					<button
						className={style["button-tab"] + " " + (!model.filterFavorites ? style["button-tab--active"] : "")}
						onClick={_ => model.setFilterFavorites(false)}
					>
						All
					</button>
					<button
						className={style["button-tab"] + " " + (model.filterFavorites ? style["button-tab--active"] : "")}
						onClick={_ => model.setFilterFavorites(true)}
					>
						Favorites
					</button>
				</div>
				<div className="row">
					<div className="col-md-8">
						<input
							className={style.search + " mb-2"}
							value={model.search}
							onChange={e => model.setSearch(e.currentTarget.value)}
							placeholder="search"
							autoFocus
						/>
					</div>
					<div className="col-md-4">
						<div className="d-flex align-items-center">
							<select
								value={model.filterType}
								onChange={e => model.setFilterType(e.currentTarget.value)}
								placeholder="type"
								className={"w-100 mb-2 me-gutter " + (model.filterType.length === 0 ? "color-placeholder" : "")}
							>
								<option value="">-- All types --</option>
								{model.pokemonTypes.map(pokemonType =>
									<option key={pokemonType} value={pokemonType}>{pokemonType}</option>
								)}
							</select>
							<div className={`${style.icon} me-2`}>
								<Image width={25} height={25} src={IconList} onClick={model.setViewList} />
							</div>
							<div className={style.icon}>
								<Image width={25} height={25} src={IconGrid} onClick={model.setViewGrid} />
							</div>
						</div>
					</div>
				</div>
			</div>
			{pokemons.count > 0 &&
				<div>
					<div className="text-center text-md-end mb-4">
						<Paginator
							className="d-block mb-2"
							limit={pokemons.limit}
							offset={pokemons.offset}
							count={pokemons.count}
							onOffsetChanged={model.reloadPage}
						/>
					</div>

					{model.view === "grid" &&
						<div className="row">
							{pokemons.edges.map(pokemon =>
								<div key={pokemon.id}
									className="col-md-4 col-lg-2 mb-gutter"
									onClick={() => router.push("/" + pokemon.name)}>
									<PokemonCard pokemon={pokemon} onChange={model.reloadPage} />
								</div>
							)}
						</div>
					}
					{model.view === "list" &&
						<ul className="pokemons-list-page__list">
							{pokemons.edges.map(pokemon =>
								<li onClick={() => router.push("/" + pokemon.name)}>
									<PokemonListItem pokemon={pokemon} onChange={model.reloadPage} />
								</li>
							)}
						</ul>
					}
				</div>
			}
			{pokemons.count == 0 && model.loadedData &&
				<p>
					No pokemons found.
				</p>
			}
			{!model.loadedData &&
				<p>
					Please wait for initial loading of data. It may take some seconds to awake sleeping server...
				</p>
			}
		</div >
	);
});