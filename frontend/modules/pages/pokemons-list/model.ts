/**
 * Model for pokemons list page
 */
import { makeAutoObservable, runInAction } from "mobx";
import { debounce } from "../../shared/common";
import * as grapqlApi from "../../shared/api/api-types-generated";
import api from "../../shared/api/api";

type View = "list" | "grid";
type Pokemons = PropType<grapqlApi.LoadPokemonsListQuery, "pokemons">;

/**
 * Page model
 */
export class Model {
	pokemons: Pokemons = {
		count: 0,
		edges: [],
		limit: 0,
		offset: 0
	};

	pokemonTypes: string[] = [];

	loadedData = false;
	search = "";
	filterType = "";
	filterFavorites = false;
	view: View = "grid";

	/**
	 * Loads pokemons
	 */
	loadPokemons = async () => {
		await this.reloadPage(0);
	}

	/**
	 * Reloads current page data
	 */
	reloadPage = async (offset: number | undefined = undefined, limit: number = 10) => {
		try {
			const result = await api.LoadPokemonsList({
				filter: {
					offset: offset ?? this.pokemons.offset,
					limit,
					search: this.search,
					filter: {
						type: this.filterType,
						isFavorite: this.filterFavorites,
					}
				}
			});

			if (result.pokemons.count > 0 && result.pokemons.edges.length === 0) {
				// skip to last nonempty page
				await this.reloadPage(Math.floor(result.pokemons.count / result.pokemons.limit));
			} else {
				runInAction(() => {
					this.pokemons = result.pokemons;
				});
			}
		} finally {
			runInAction(() => {
				this.loadedData = true;
			});
		}
	}

	/**
	 * Load pokemons debounced
	 */
	loadPokemonsDebounced = debounce(this.loadPokemons, 250);

	/**
	 * Loads pokemon types
	 */
	loadPokemonTypes = async () => {
		const result = await api.LoadPokemonTypes();
		runInAction(() => {
			this.pokemonTypes = result.pokemonTypes;
		});
	}

	/**
	 * Set search phrase 
	 */
	setSearch = (search: string) => {
		this.search = search;
		this.loadPokemonsDebounced();
	}

	/**
	 * Set filter by pokemon type 
	 */
	setFilterType = async (filterType: string) => {
		this.filterType = filterType;
		await this.loadPokemons();
	}

	/**
	 * Set filter for favorites
	 */
	setFilterFavorites = async (filterFavorites: boolean) => {
		this.filterFavorites = filterFavorites;
		await this.loadPokemons();
	}

	/**
	 * Set grid view
	 */
	setViewGrid = async () => {
		this.view = "grid";
	}

	/**
	 * Set list view
	 */
	setViewList = async () => {
		this.view = "list";
	}

	/**
	 * Loads page data
	 */
	fetch = async () => {
		await Promise.all([
			this.loadPokemonTypes(),
			this.reloadPage()
		]);
	}

	constructor() {
		makeAutoObservable(this);
	}
}

const pokemonsList = new Model();
export default pokemonsList;