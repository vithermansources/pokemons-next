/**
 * Pokemon list item component to show in list page
 */

import pokemonDialog from "../../shared/pokemons/dialog/model";
import { togglefavorite } from "../../shared/pokemons/utils";

import Image from "next/image";
import IconEye from "../../../public/images/icon-eye.png";
import IconHeart from "../../../public/images/heart.png";
import IconFullHeart from "../../../public/images/fullheart.png";

import style from "./PokemonListItem.module.scss";

interface Pokemon {
	id: string;
	name: string;
	types: string[];
	isFavorite: boolean;
	image: string;
}

export interface PokemonListItemProps {
	pokemon: Pokemon;
	onChange: () => void;
}

export default function PokemonListItem(props: PokemonListItemProps) {

	function onChange() {
		if (props.onChange) {
			props.onChange();
		}
	}

	return (
		<div className={style.item + " row"}>
			<div className="col-md-4 align-self-center text-center text-md-left mb-2 mb-md-0">
				<Image width={60} height={60} src={props.pokemon.image} />
			</div>
			<div className="col-md-4 align-self-center text-center text-md-left">
				<strong>{props.pokemon.name}</strong><br />
				{props.pokemon.types.join(", ")}
			</div>
			<div className="col-md-4 d-flex align-items-center align-self-center justify-content-center justify-content-md-end">
				<div className={style.icon + " me-2"}>
					<Image width={30} height={30} src={IconEye} onClick={async e => {
						e.stopPropagation();
						await pokemonDialog.show(props.pokemon.id, onChange);
					}} />
				</div>
				<div className={style.icon}>
					<Image
						width={20}
						height={20}
						src={props.pokemon.isFavorite ? IconFullHeart : IconHeart}
						onClick={async (e) => {
							e.stopPropagation();
							await togglefavorite(props.pokemon);
							onChange();
						}}
					/>
				</div>
			</div>
		</div>
	);
}