import style from "./Paginator.module.scss";

/**
 * Paginator
 */
export interface PaginatorProps {
	className?: string;
	limit: number;
	offset: number;
	count: number;
	onOffsetChanged: (offset: number) => Promise<void>;
}

export default function Pagination(props: PaginatorProps) {

	async function handleOffsetChanged(offset: number) {
		await props.onOffsetChanged(offset);
	}

	let pagesCount = Math.ceil(props.count / props.limit);
	if (pagesCount == 0)
		pagesCount = 1;

	if (pagesCount === 1) {
		return null;
	}

	return (
		<span className={style.paginator + " " + (props.className ?? "")}>
			<button
				className={style.button + " mr-1"}
				disabled={props.offset == 0}
				onClick={_ => handleOffsetChanged(0)}
			>
				&lt;&lt;
			</button>

			<button
				className={style.button}
				disabled={props.offset == 0}
				onClick={_ => handleOffsetChanged(props.offset - props.limit)}
			>
				&lt;
			</button>

			<span className={style.page + " px-3"}>
				{Math.floor(props.offset / props.limit) + 1} / {pagesCount}
			</span>

			<button
				className={style.button + " mr-1"}
				disabled={props.count - props.offset < props.limit}
				onClick={_ => handleOffsetChanged(props.offset + props.limit)}
			>
				&gt;
			</button>

			<button
				className={style.button}
				disabled={props.count - props.offset < props.limit}
				onClick={_ => handleOffsetChanged((pagesCount - 1) * props.limit)}
			>
				&gt;&gt;
			</button>
		</span>
	);
}