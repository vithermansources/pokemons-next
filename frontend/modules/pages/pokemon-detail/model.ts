/**
 * Model of pokemon's detail
 */
import { makeAutoObservable, runInAction } from "mobx";
import * as grapqlApi from "../../shared/api/api-types-generated";
import api from "../../shared/api/api";

export type Pokemon = PropType<grapqlApi.LoadPokemonByIdQuery, "pokemonById">;

export default class Model {
	pokemon: Pokemon;

	constructor() {
		makeAutoObservable(this);
	}

	load = async (pokemonName: string) => {
		const pokemon = await api.LoadPokemonByName({ name: pokemonName });
		runInAction(() => {
			this.pokemon = pokemon.pokemonByName;
		});
	}

	set = (pokemon: Pokemon) => {
		this.pokemon = pokemon;
	}

	reload = async () => {
		if (!this.pokemon) return;
		await this.load(this.pokemon.name);
	}
}