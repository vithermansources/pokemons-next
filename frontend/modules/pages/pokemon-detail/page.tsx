/**
 * Pokemon's detail page
 * 
 * Shows detail page of pokemon. Creates one state model per component instance.
 */
import { GetStaticPropsContext, InferGetServerSidePropsType } from "next";
import { useState, useEffect } from "react";
import { observer } from "mobx-react";

import Model from "./model";

import PokemonDetail from "../../shared/pokemons/PokemonDetail";


export default observer(function PokemonDetailPage(props: InferGetServerSidePropsType<typeof getServerSideProps>) {

	// create new model after component mount
	const [pokemonDetailModel] = useState(new Model());

	useEffect(() => {
		// init state model from props (fullfiled with server side rendered data)
		pokemonDetailModel.set(props.pokemon);
	}, []);

	return (
		<div>
			{pokemonDetailModel.pokemon &&
				<PokemonDetail pokemon={pokemonDetailModel.pokemon} onChange={pokemonDetailModel.reload} />
			}
		</div>
	);
});

/**
 * Server side rendered data
 */
export const getServerSideProps = async (context: GetStaticPropsContext) => {
	const pokemonModel = new Model();
	const name = context.params?.name;
	if (typeof name === "string") {
		await pokemonModel.load(name);
	}

	return {
		props: {
			pokemon: pokemonModel.pokemon
		}
	};
};